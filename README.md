# Awesome Recipes

This project contains the challenge of trying to make a simple app web about recipes with React with no more than 4 hours.

The entire project took me arround 5 hours. The last hour was used to implement all the favorite features (points 7, 8 and 9 of the next list) and refactor the code (also I added a new funcitonality to see the tags of every recipe :D).

## Objetives achieved 100% with 4 hours

1) Integrate with "TheMealDB" API http://www.themealdb.com/api.php

2) The main page will display 5 random recipes as 'Recipes of the Day'
Mobile designs are provided, get creative with responsive desktop designs

3) Use a css preprocessor (*SCSS*)

4) Clicking on a recipe should open a detail page about the recipe
The URL should change (consider using react-router) when a recipe is clicked

5) If a user copy/pastes the URL into a new window, the same recipe detail page should display

6) Detail page should show the ingredients and instructions for that recipe
There should be a search bar allowing users to search for a recipe

## Objetives achieved 100% with 5 hours

7) Add a 'Heart' icon in the modal for a recipe

8) The heart icon should 'favorite' this recipe. Store this data in the users browser

9) If any favorited recipes are detected in the browser, add a button on the home page that shows a list of these recipes

## Objectives partially achieved

1) Search button on mobile in the bottom right corner of the screen - which should open a search window with the keyboard. The search results will be displayed where the random recipes were displayed

* This is totally implemented on the desktop version, but not in the mobile view. With a little of more time this could be finished as well

## Objectives not implemented

1) Accessibility.

* I couldn´t implement Accessibility in the time. Im familiar with the accessibility features on mobile projects (react-native) but not in reactJS, so for implement this I should have researched how this work. To be completely transparent with the test, I couldn't have made it inside the 4 hours, but with more time should't be a problem.

### Instructions
### `npm install && npm start`

#### Thanks!.