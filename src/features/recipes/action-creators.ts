import axios from "axios";
import {
  FETCH_RECIPES_FAILURE,
  FETCH_RECIPES_REQUEST,
  FETCH_RECIPES_SUCCESS,
} from "./actions";
import { Recipe } from "./reducer";

export const fetchRecipeRequest = () => {
  return {
    type: FETCH_RECIPES_REQUEST,
  };
};
export const fetchRecipeFailure = (error: string) => {
  return {
    type: FETCH_RECIPES_FAILURE,
    payload: error,
  };
};

export const fetchRecipeSuccess = (recipes: Recipe[]) => {
  return {
    type: FETCH_RECIPES_SUCCESS,
    payload: recipes,
  };
};

export const getOneRecipeData = async (url: string) => {
  const response = await axios.get(url);
  const meals = response.data.meals;
  const currentMeal = meals[0];
  return currentMeal;
};

export const searchMultipleRecipesData = async (url: string) => {
  const response = await axios.get(url);
  const meals = response.data.meals;
  return meals;
};

export const formatRecipe = (currentRecipe: any) => {
  let formattedMeal: Recipe = {
    id: currentRecipe.idMeal,
    title: currentRecipe.strMeal,
    category: currentRecipe.strCategory,
    area: currentRecipe.strArea,
    instructions: currentRecipe.strInstructions,
    tags: currentRecipe.strTags ? currentRecipe.strTags.split(",") : [],
    ingredients: [],
    measures: [],
    imageUrl: currentRecipe.strMealThumb,
  };

  for (const [k, v] of Object.entries(currentRecipe)) {
    if (k.includes("strIngredient")) {
      if (typeof v == "string") formattedMeal.ingredients.push(v);
    } else if (k.includes("strMeasure")) {
      if (typeof v == "string") formattedMeal.measures.push(v);
    }
  }
  return formattedMeal;
};

export const fetchRecipes = () => {
  return async (dispatch: any) => {
    dispatch(fetchRecipeRequest());
    try {
      const cacheAdded = new Set<string>();
      const groupOfRandomMeals = [];
      while (groupOfRandomMeals.length !== 5) {
        // To add only uniques recipes
        const meal = await getOneRecipeData(
          "https://www.themealdb.com/api/json/v1/1/random.php"
        );
        if (!cacheAdded.has(meal.idMeal)) {
          groupOfRandomMeals.push(formatRecipe(meal));
        }
      }
      dispatch(fetchRecipeSuccess(groupOfRandomMeals));
    } catch (error) {
      dispatch(fetchRecipeFailure(error));
    }
  };
};
