import {
  FETCH_RECIPES_FAILURE,
  FETCH_RECIPES_REQUEST,
  FETCH_RECIPES_SUCCESS,
} from "./actions";

export type Recipe = {
  id: string;
  title: string;
  category: string;
  area: string;
  instructions: string;
  tags: string[];
  ingredients: string[];
  measures: string[];
  imageUrl: string;
};

interface RecipeState {
  recipes: Recipe[];
  loading: boolean;
  error: string | null;
}

const initialState: RecipeState = {
  recipes: [],
  loading: false,
  error: "",
};

export default function recipeReducer(
  state = initialState,
  action: any
): RecipeState {
  switch (action.type) {
    case FETCH_RECIPES_REQUEST:
      return { ...state, loading: true };
    case FETCH_RECIPES_FAILURE:
      return { recipes: state.recipes, loading: false, error: action.payload };
    case FETCH_RECIPES_SUCCESS:
      return {
        ...state,
        loading: false,
        recipes: [...state.recipes, ...action.payload],
      };
    default:
      return { ...state };
  }
}
