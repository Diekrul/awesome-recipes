import React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import { RecipeDetail } from "./components/RecipeDetail";
import RecipesList from "./components/RecipesList";

function App() {
  return (
    <BrowserRouter>
      <Switch>
        <Route path="/recipe/:id">
          <RecipeDetail />
        </Route>
        <Route exact path="/">
          <RecipesList />
        </Route>
      </Switch>
    </BrowserRouter>
  );
}

export default App;
