import { faArrowLeft, faHeart } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { useEffect, useState } from "react";
import { useHistory, useParams } from "react-router-dom";
import {
  formatRecipe,
  getOneRecipeData,
} from "../features/recipes/action-creators";
import { Recipe } from "../features/recipes/reducer";
import "./../style/header.scss";
import "./../style/recipe-detail.scss";
import Header from "./layout/Header";

interface RecipeDetailNavigationProps {
  id: string;
}

interface RecipeInfoStorage {
  id: string;
  title: string;
  imageUrl: string;
  tags: string[];
}
const FAVORITES = "favorites";

export const RecipeDetail = () => {
  const { id } = useParams<RecipeDetailNavigationProps>();
  const history = useHistory();
  const [currentRecipe, setCurrentRecipe] = useState<Recipe>();

  useEffect(() => {
    async function fetchData() {
      const searchUrl = `https://www.themealdb.com/api/json/v1/1/lookup.php?i=${id}`;
      const recipe = await getOneRecipeData(searchUrl);
      setCurrentRecipe(formatRecipe(recipe));
    }
    fetchData();
  }, [id]);

  const handleFavorites = () => {
    const storedFavorites = localStorage.getItem(FAVORITES);

    if (currentRecipe) {
      const recipeInfo: RecipeInfoStorage = {
        id: currentRecipe.id,
        title: currentRecipe.title,
        imageUrl: currentRecipe.imageUrl,
        tags: currentRecipe.tags,
      };
      if (storedFavorites) {
        const storedFavoritesArray: RecipeInfoStorage[] = JSON.parse(
          storedFavorites
        );
        //check if element exists in favorites
        const indexOfElementInFavorites = storedFavoritesArray.findIndex(
          (recipe: RecipeInfoStorage) => recipe.id === currentRecipe.id
        );
        if (indexOfElementInFavorites !== -1) {
          //If exists, we remove the element
          storedFavoritesArray.splice(indexOfElementInFavorites, 1);
          localStorage.setItem(FAVORITES, JSON.stringify(storedFavoritesArray));
        } else {
          //If not, we add the element into favorites
          storedFavoritesArray.push(recipeInfo);
          localStorage.setItem(FAVORITES, JSON.stringify(storedFavoritesArray));
        }
      } else {
        localStorage.setItem(FAVORITES, JSON.stringify([recipeInfo]));
      }
    }
  };

  return currentRecipe ? (
    <>
      <div className="recipe-detail-header">
        <Header />
      </div>
      <div className="navBar">
        <div className="arrow">
          <FontAwesomeIcon
            onClick={() => history.goBack()}
            color={"white"}
            size={"2x"}
            icon={faArrowLeft}
          />
        </div>
        <div className="heart">
          <FontAwesomeIcon
            color={"white"}
            size={"2x"}
            icon={faHeart}
            onClick={() => handleFavorites()}
          />
        </div>
      </div>

      <div className="recipe-big-title">
        <h1>{currentRecipe.title}</h1>
        <div className="heart">
          <FontAwesomeIcon
            color={"red"}
            size={"2x"}
            icon={faHeart}
            onClick={() => handleFavorites()}
          />
        </div>
      </div>
      <div className="recipe-detail">
        <div className="recipe-detail-image-container">
          <img
            className="recipe-detail-image"
            src={currentRecipe.imageUrl}
            alt="header"
          />
        </div>
        <div className="recipe-detail-content">
          <div className="recipe-detail-content-content">
            <ul>
              {currentRecipe.ingredients.map((ing) =>
                ing !== "" ? <li>{ing}</li> : null
              )}
            </ul>
          </div>
          <div className="recipe-detail-content-instructions">
            <h2>Instructions</h2>
          </div>
          <div className="recipe-detail-content-detail">
            {currentRecipe.instructions}
          </div>
        </div>
      </div>
    </>
  ) : null;
};
