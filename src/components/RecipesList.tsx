import { faSearch, faWindowClose } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  fetchRecipes,
  formatRecipe,
  searchMultipleRecipesData,
} from "../features/recipes/action-creators";
import { Recipe } from "../features/recipes/reducer";
import { RootState } from "../features/recipes/store";
import "./../style/recipes.scss";
import Header from "./layout/Header";
import { RecipeItem } from "./RecipeItem";

export default function RecipesList() {
  const dispatch = useDispatch();
  const { recipes, loading, error } = useSelector(
    (state: RootState) => state.recipes
  );
  const [filteredRecipes, setFilteredRecipes] = useState<Recipe[]>();
  const [currentSearchedValue, setCurrentSearchedValue] = useState<string>("");
  const [isFavoriteOn, setIsFavoriteOn] = useState(false);

  useEffect(() => {
    if (recipes.length !== 5) {
      dispatch(fetchRecipes());
    }
  }, [dispatch, recipes.length]);

  const showFavorites = () => {
    const storedFavorites = localStorage.getItem("favorites");
    if (storedFavorites) {
      setFilteredRecipes(JSON.parse(storedFavorites));
      setIsFavoriteOn(true);
    }
  };
  const hideFavorites = () => {
    setFilteredRecipes(undefined);
    setIsFavoriteOn(false);
  };

  const filterValuesBySearch = async () => {
    const fetchData = async () => {
      const searchUrl = `https://www.themealdb.com/api/json/v1/1/search.php?s=${currentSearchedValue}`;
      const recipes = await searchMultipleRecipesData(searchUrl);
      const recipesWithFormat: Recipe[] = [];
      if (recipes) {
        recipes.forEach((element: Recipe) => {
          recipesWithFormat.push(formatRecipe(element));
        });
      }
      setFilteredRecipes(recipesWithFormat);
    };
    if (currentSearchedValue !== "") {
      fetchData();
    } else {
      setFilteredRecipes(undefined);
    }
  };

  const valuesToRender = filteredRecipes ? filteredRecipes : recipes;
  return loading ? (
    <h1>Loading...</h1>
  ) : error ? (
    <h1>Error</h1>
  ) : (
    <>
      <Header />
      <div className="recipe-big-title">
        <h1>Recipes of the day</h1>
      </div>
      <div className="search-bar">
        <input
          className="search-bar-input"
          placeholder="Search a recipe..."
          type="text"
          value={currentSearchedValue}
          onChange={(e) => setCurrentSearchedValue(e.target.value)}
        />
        <div className="search-bar-close">
          <FontAwesomeIcon
            onClick={() => {
              setFilteredRecipes(undefined);
              setCurrentSearchedValue("");
            }}
            color={"red"}
            size={"1x"}
            icon={faWindowClose}
          />
        </div>
        <button
          className="search-performance-action"
          onClick={() => filterValuesBySearch()}
        >
          Search
        </button>

        <button
          className="favorites-button"
          onClick={() => (!isFavoriteOn ? showFavorites() : hideFavorites())}
        >
          {!isFavoriteOn ? "Show Favorites" : "Hide Favorites"}
        </button>
      </div>
      <div className="recipes">
        {valuesToRender.map(({ id, title, tags, imageUrl }) => {
          return (
            <RecipeItem
              id={id}
              key={id}
              title={title}
              tags={tags}
              url={imageUrl}
            />
          );
        })}
      </div>
      <div className="search-button">
        <FontAwesomeIcon
          onClick={() => {}}
          color={"white"}
          size={"2x"}
          icon={faSearch}
        />
      </div>
    </>
  );
}
