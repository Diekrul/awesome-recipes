import React, { FC } from "react";
import { useHistory } from "react-router-dom";

interface RecipeItemProps {
  id: string;
  title: string;
  tags: string[];
  url: string;
}

const RecipeItemInteral: FC<RecipeItemProps> = ({ id, title, tags, url }) => {
  const history = useHistory();
  const navigateToDetail = () => {
    history.push(`/recipe/${id}`);
  };
  return (
    <div className="recipe" onClick={() => navigateToDetail()}>
      <div className="recipe-title">{title}</div>
      <div className="recipe-image">
        <img className="image" src={url} alt="recipe" />
      </div>

      <div className="recipe-tags">
        {tags.map((tag, index) => {
          return tag ? (
            <div key={`${id}${index}`} className="tag">
              {tag}
            </div>
          ) : null;
        })}
      </div>
    </div>
  );
};
export const RecipeItem = React.memo(RecipeItemInteral);
