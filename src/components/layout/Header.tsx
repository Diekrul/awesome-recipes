import React from "react";
import "./../../style/header.scss";
const headerUrl =
  "https://github.com/EverlyWell/react-challenge/blob/master/assets/home-background.jpg?raw=true";
const logo =
  "https://raw.githubusercontent.com/EverlyWell/react-challenge/master/assets/logo.png";

export default function Header() {
  return (
    <div className="header">
      <div className="logo">
        <img src={logo} alt="logo" />
      </div>
      <img className="header-image" src={headerUrl} alt="header" />
    </div>
  );
}
